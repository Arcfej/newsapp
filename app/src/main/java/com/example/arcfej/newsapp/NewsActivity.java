package com.example.arcfej.newsapp;

import android.app.LoaderManager;
import android.content.Loader;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

public class NewsActivity extends AppCompatActivity
        implements LoaderManager.LoaderCallbacks<List<News>> {

    private static final int NEWS_LOADER_ID = 101;
    private static final String SEARCH_QUERY = "comics AND marvel";

    NewsAdapter adapter;
    TextView emptyListView;
    ProgressBar loadingIndicator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        //region Setup the RecyclerView
        RecyclerView list = findViewById(R.id.news_list);
        adapter = new NewsAdapter();
        list.setAdapter(adapter);
        LinearLayoutManager layoutManager =
                new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        list.setLayoutManager(layoutManager);
        list.addItemDecoration(new DividerItemDecoration(this, layoutManager.getOrientation()));
        //endregion
        emptyListView = findViewById(R.id.empty_list_view);
        loadingIndicator = findViewById(R.id.loading_indicator);
        // Start downloading or showing the list of news.
        getLoaderManager().initLoader(NEWS_LOADER_ID, null, this);
    }

    /**
     * If the app has internet connection, starts downloading the list of news
     * and shows the progress to the user.
     * If not, shows a "no connection" message.
     *
     * @param id   is not used
     * @param args is not used
     * @return the loader which will download the list from the server.
     */
    @Override
    public Loader<List<News>> onCreateLoader(int id, Bundle args) {
        adapter.clearList();
        if (hasConnection()) {
            emptyListView.setVisibility(View.INVISIBLE);
            loadingIndicator.setVisibility(View.VISIBLE);
            return new NewsLoader(getApplicationContext(), SEARCH_QUERY);
        } else {
            showNoInternetMessage();
            return null;
        }
    }

    /**
     * Shows a "no connection" message to the user. It is not hide the list, it should be empty.
     */
    private void showNoInternetMessage() {
        loadingIndicator.setVisibility(View.INVISIBLE);
        emptyListView.setText(R.string.no_connection_message);
        emptyListView.setVisibility(View.VISIBLE);
    }

    /**
     * Checks if the app has internet access or not.
     *
     * @return true if it has.
     */
    private boolean hasConnection() {
        ConnectivityManager cm =
                (ConnectivityManager) this.getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = null;
        if (cm != null) {
            activeNetwork = cm.getActiveNetworkInfo();
        }
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    /**
     * This method"s called when the download is finished. If the list is empty, tell the user there
     * is no results. Otherwise show the result to the user.
     *
     * @param loader is not used
     * @param data:  the list of news
     */
    @Override
    public void onLoadFinished(Loader<List<News>> loader, List<News> data) {
        loadingIndicator.setVisibility(View.INVISIBLE);
        if (data.isEmpty()) {
            emptyListView.setText(R.string.no_results_message);
            emptyListView.setVisibility(View.VISIBLE);
        } else {
            adapter.addListToList(data);
        }
    }

    /**
     * Clear the adapter's data
     *
     * @param loader is not used
     */
    @Override
    public void onLoaderReset(Loader<List<News>> loader) {
        adapter.clearList();
    }

    /**
     * Inflate the menu for the activity
     *
     * @param menu to inflate into
     * @return true
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    /**
     * If the clicked menu:
     * - REFRESH: restart the loader to download news, or initialize it if it isn't done yet.
     *
     * @param item which is clicked
     * @return false if none of the items above clicked
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == R.id.refresh) {
            if (hasConnection()) {
                if (getLoaderManager().getLoader(NEWS_LOADER_ID) == null) {
                    getLoaderManager().initLoader(NEWS_LOADER_ID, null, this);
                } else {
                    getLoaderManager().restartLoader(NEWS_LOADER_ID, null, this);
                }
                return true;
            } else {
                adapter.clearList();
                showNoInternetMessage();
            }
        }
        return super.onOptionsItemSelected(item);
    }
}