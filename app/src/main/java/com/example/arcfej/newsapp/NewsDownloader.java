package com.example.arcfej.newsapp;

import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * This class contains helper methods to download news from The Guardian's server.
 */

public class NewsDownloader {

    private static final String LOG_TAG = "NewsDownloader.class";

    //region This constants needed for building the request URL
    private static final String THE_GUARDIAN_URI = "http://content.guardianapis.com/search";
    private static final String QUERY_PARAMETER = "q";
    private static final String SHOW_TAGS_PARAMETER = "show-tags";
    private static final String SHOW_TAGS_VALUE = "contributor";
    private static final String KEY_PARAMETER = "api-key";
    private static final String KEY_VALUE = "test";
    private static final String ORDER_BY_PARAMETER = "order-by";
    private static final String ORDER_BY_VALUE = "newest";
    //endregion

    /**
     * Required private constructor, so nobody can create an instance of this class.
     */
    private NewsDownloader() {}

    /**
     * Download a list of News from the Guardian server.
     *
     * @param query is the topic of the news which this method downloads.
     * @return the list of News.
     */
    public static List<News> downloadNewsList(String query) {
        URL url = null;
        try {
            url = buildUrl(query);
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Cannot build url");
        }
        String jsonResponse = downloadJsonData(url);
        return createNewsList(jsonResponse);
    }

    /**
     * Build a request URL to the The Guardian server with the given query.
     *
     * @param query is the topic we want to download news related to.
     * @return the request URL or null if the query is empty or null.
     * @throws MalformedURLException if it couldn't build the URL.
     */
    private static URL buildUrl(String query) throws MalformedURLException {
        if (TextUtils.isEmpty(query)) {
            return null;
        }
        Uri builtUri = Uri.parse(THE_GUARDIAN_URI).buildUpon()
                .appendQueryParameter(QUERY_PARAMETER, query)
                .appendQueryParameter(SHOW_TAGS_PARAMETER, SHOW_TAGS_VALUE)
                .appendQueryParameter(ORDER_BY_PARAMETER, ORDER_BY_VALUE)
                .appendQueryParameter(KEY_PARAMETER, KEY_VALUE)
                .build();
        return new URL(builtUri.toString());
    }

    /**
     * Download news from The Guardian server in JSON format.
     *
     * @param url is the request url to download from.
     * @return the downloaded JSON result in String, or an empty String if there was no result.
     */
    private static String downloadJsonData(URL url) {
        if (url == null) {
            return "";
        }
        String jsonResponse = "";
        HttpURLConnection connection = null;
        InputStream inStream = null;
        Scanner scanner = null;
        try {
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setConnectTimeout(15000);
            connection.setReadTimeout(20000);
            connection.connect();
            int responseCode = connection.getResponseCode();
            if (responseCode != 200) {
                Log.e(LOG_TAG, "HttpUrlConnection response code: " + responseCode);
                Log.e(LOG_TAG, "HttpURLConnection response message: "
                        + connection.getResponseMessage());
                return "";
            }
            inStream = connection.getInputStream();
            scanner = new Scanner(inStream);
            jsonResponse = scanner.useDelimiter("\\A").next();
        } catch (IOException e) {
            Log.e(LOG_TAG, "Cannot connect to The Guardian server");
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
            if (inStream != null) {
                try {
                    inStream.close();
                } catch (IOException e) {
                    Log.e(LOG_TAG, "Cannot close input stream");
                }
            }
            if (scanner != null) {
                scanner.close();
            }
        }
        return jsonResponse;
    }

    /**
     * Fetch the News from the downloaded JSON data and create a list of News from them.
     *
     * @param jsonString is the downloaded data in String.
     * @return the list of News, or an empty list if it cannot fetch the News from the jsonString.
     */
    private static List<News> createNewsList(String jsonString) {
        if (TextUtils.isEmpty(jsonString)) {
            return new ArrayList<>();
        }
        List<News> newsList = new ArrayList<>();
        try {
            JSONObject response = new JSONObject(jsonString).getJSONObject("response");
            JSONArray results = response.getJSONArray("results");
            int length = response.getInt("pageSize");
            for (int i = 0; i < length; i++) {
                JSONObject item = results.getJSONObject(i);
                String title = item.optString("webTitle");
                String sectionName = item.optString("sectionName");
                JSONArray tags = item.getJSONArray("tags");
                List<String> authors = new ArrayList<>();
                for (int j = 0; j < tags.length(); j++) {
                    authors.add(tags.getJSONObject(j).optString("webTitle"));
                }
                String date = item.optString("webPublicationDate");
                String webUrl = item.optString("webUrl");
                newsList.add(new News(title, sectionName, authors,
                        Formatters.stringDateToSeconds(date), webUrl));
            }
        } catch (JSONException e) {
            Log.e(LOG_TAG, "Cannot parse JSONObject or -Array");
        } catch (ParseException e) {
            Log.e(LOG_TAG, "Cannot parse date from String");
        }
        return newsList;
    }
}