package com.example.arcfej.newsapp;

import android.text.TextUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * This class has methods which format dates
 */

public class Formatters {

    /**
     * Required private constructor, so nobody can create an instance of this class.
     */
    private Formatters() {}

    /**
     * Convert the downloaded JSON date format from the The Guardian into Unix time in seconds
     *
     * @param stringDate is the date in String and '2017-12-29T15:42:09Z' format
     * @return the converted time in seconds
     * @throws ParseException if it cannot parse the String into Date.
     */
    public static long stringDateToSeconds(String stringDate) throws ParseException {
        if (TextUtils.isEmpty(stringDate)) {
            return 0;
        }
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss'Z'");
        Date date = format.parse(stringDate);
        return date.getTime() / 1000L;
    }

    /**
     * Convert a date in Unix seconds to 'MM.dd hh:mm' date format.
     *
     * @param seconds to convert into String
     * @return the date in String.
     */
    public static String unixTimeToString(long seconds) {
        if (seconds == 0) {
            return "";
        }
        DateFormat formatter = new SimpleDateFormat("MM.dd hh:mm");
        Date date = new Date(seconds * 1000L);
        return formatter.format(date);
    }
}
