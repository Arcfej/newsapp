package com.example.arcfej.newsapp;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Display a list of news to the user.
 */

class NewsAdapter extends android.support.v7.widget.RecyclerView.Adapter<NewsAdapter.NewsHolder> {

    // The list to display
    private List<News> newsList;

    /**
     * Constructor of the class.
     */
    public NewsAdapter() {
        newsList = new ArrayList<>();
    }

    /**
     * ViewHolder for news. Handle click events.
     */
    public class NewsHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        //region Views of the ViewHolder
        private TextView title;
        private TextView sectionName;
        private TextView authors;
        private TextView date;
        //endregion
        // The web url of the displayed news. It's needed for the OnClickListener
        private String webUrl;

        /**
         * Constructor of the class. Set an OnClickListener on the itemView.
         *
         * @param itemView is the inflated view to display data in
         */
        public NewsHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.tv_title);
            sectionName = itemView.findViewById(R.id.tv_section);
            authors = itemView.findViewById(R.id.tv_authors);
            date = itemView.findViewById(R.id.tv_date);
            webUrl = "";
            itemView.setOnClickListener(this);
        }

        /**
         * Open a webpage with the webUrl, if it's not empty.
         *
         * @param v is the clicked view
         */
        @Override
        public void onClick(View v) {
            if (TextUtils.isEmpty(webUrl)) {
                Toast.makeText(v.getContext(), R.string.no_webpage_message, Toast.LENGTH_SHORT)
                        .show();
            } else {
                Intent openWeb = new Intent(Intent.ACTION_VIEW, Uri.parse(webUrl));
                Context context = itemView.getContext();
                if (openWeb.resolveActivity(context.getPackageManager()) != null) {
                    context.startActivity(openWeb);
                }
            }
        }
    }

    /**
     * Create a new ViewHolder for the adapter.
     *
     * @param parent   of the ViewHolder
     * @param viewType is not used
     * @return the new NewsHolder
     */
    @Override
    public NewsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflatedView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.news_list_item, parent, false);
        return new NewsHolder(inflatedView);
    }

    /**
     * Display the item's data and set the url to open when the item's clicked.
     *
     * @param holder   to bind the data to.
     * @param position of the item
     */
    @Override
    public void onBindViewHolder(NewsHolder holder, int position) {
        Context activityContext = holder.itemView.getContext();
        News news = newsList.get(position);
        holder.title.setText(news.getTitle());
        holder.sectionName.setText(news.getSectionName());
        //region The authors end date View visibility set to Gone if there's no value for them.
        String authorsString = fetchAuthors(news.getAuthors());
        if (TextUtils.isEmpty(authorsString)) {
            holder.authors.setVisibility(View.GONE);
        } else {
            authorsString = activityContext.getString(R.string.authors, authorsString);
            CharSequence styledAuthorsString = Html.fromHtml(authorsString);
            holder.authors.setText(styledAuthorsString);
            holder.authors.setVisibility(View.VISIBLE);
        }
        String date = Formatters.unixTimeToString(news.getDate());
        if (TextUtils.isEmpty(date)) {
            holder.date.setVisibility(View.GONE);
        } else {
            holder.date.setText(date);
            holder.date.setVisibility(View.VISIBLE);
        }
        //endregion
        holder.webUrl = news.getUrlString();
    }

    /**
     * Create a single String object from a List of authors.
     *
     * @param authors as a list of Strings
     * @return "", if the list is empty, and the authors in a String separated with comas, if not.
     */
    private String fetchAuthors(List<String> authors) {
        switch (authors.size()) {
            case 0:
                return "";
            case 1:
                return authors.get(0);
            default:
                StringBuilder authorsString = new StringBuilder(authors.get(0));
                for (int i = 1; i < authors.size(); i++) {
                    authorsString.append(", ")
                            .append(authors.get(i));
                }
                return authorsString.toString();
        }
    }

    /**
     * @return the count of the list's items
     */
    @Override
    public int getItemCount() {
        return newsList.size();
    }

    /**
     * Clear the list so the items count become 0
     */
    public void clearList() {
        newsList.clear();
        notifyDataSetChanged();
    }

    /**
     * Add new items to the end of the list
     *
     * @param newItems is the list of News to add
     */
    public void addListToList(List<News> newItems) {
        newsList.addAll(newItems);
        notifyItemRangeInserted(getItemCount(), newItems.size());
    }
}