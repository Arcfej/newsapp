package com.example.arcfej.newsapp;

import android.content.AsyncTaskLoader;
import android.content.Context;

import java.util.List;

/**
 * Download a list of News from The Guardian server on a background thread.
 */

public class NewsLoader extends AsyncTaskLoader<List<News>> {

    // The topic which the news search in.
    private final String query;
    // Cash the downloaded list for later use.
    private List<News> newsList;

    /**
     * Constructor of the NewsLoader.
     *
     * @param context of the app.
     * @param query   is the topic of the news the user wants to download.
     */
    public NewsLoader(Context context, String query) {
        super(context);
        this.query = query;
    }

    /**
     * Force the download to start when the Loader created.
     */
    @Override
    protected void onStartLoading() {
        if (newsList == null) {
            forceLoad();
        } else {
            deliverResult(newsList);
        }
    }

    /**
     * Download the list of News from the The Guardian server in a background thread and
     *
     * @return it to the caller activity.
     */
    @Override
    public List<News> loadInBackground() {
        return NewsDownloader.downloadNewsList(query);
    }

    /**
     *
     * @param data
     */
    @Override
    public void deliverResult(List<News> data) {
        newsList = data;
        super.deliverResult(data);
    }
}