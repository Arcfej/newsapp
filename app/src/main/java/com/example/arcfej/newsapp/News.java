package com.example.arcfej.newsapp;

import java.util.List;

/**
 * This model holds information about a news
 */

class News {

    private String sectionName;
    private String title;
    private List<String> authors;
    // The date in Unix time in seconds. Default value should be 0.
    private long date;
    private String urlString;

    public News(String title, String sectionName, List<String> authors, long date, String urlString) {
        this.title = title;
        this.sectionName = sectionName;
        this.authors = authors;
        this.date = date;
        this.urlString = urlString;
    }

    public String getTitle() {
        return title;
    }

    public String getSectionName() {
        return sectionName;
    }

    public List<String> getAuthors() {
        return authors;
    }

    public long getDate() {
        return date;
    }

    public String getUrlString() {
        return urlString;
    }
}